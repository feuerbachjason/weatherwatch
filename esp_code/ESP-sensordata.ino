/*
Author: Jason Feuerbach, Jan Knierim, Sebastian Wieczorek
Pushing sensordata to mqtt
*/
#include "ESP8266WiFi.h"
#include <PubSubClient.h>
#include "DHT.h"


//Pinout
#define DHTPIN 4 //D2
#define LDRPIN 5  //D1
#define DHTTYPE DHT22
#define MSG_BUFFER_SIZE  (80)

//Constants and variables
String clientID = "ESP_Wetterstation";
const char* mqtt_ip = "192.168.178.88";
const int mqtt_port = 1883;
const char* topic = "/weatherwatch/data/";
unsigned long lastMsg = 0;
char msg[MSG_BUFFER_SIZE];
byte willQoS = 0;
const char* onlineTopic = "/weatherwatch/online/";
const char* willMessage = "{\"status\": \"0\"}";
const char* isOnlineMessage = "{\"status\": \"1\"}";
boolean willRetain = false;



//create objects
WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE);

//Startet die WPS Konfiguration
bool startWPS() {
  Serial.println("WPS Konfiguration gestartet");
  bool wpsSuccess = WiFi.beginWPSConfig();
  if(wpsSuccess) {
      // Muss nicht immer erfolgreich heißen! Nach einem Timeout ist die SSID leer
      String newSSID = WiFi.SSID();
      if(newSSID.length() > 0) {
        // Nur wenn eine SSID gefunden wurde waren wir erfolgreich 
        Serial.printf("WPS fertig. Erfolgreich angemeldet an SSID '%s'\n", newSSID.c_str());
      } else {
        wpsSuccess = false;
      }
  }
  return wpsSuccess; 
}

void setup_wifi(){
  Serial.printf("\nVersuche Verbindung mit gespeicherter SSID '%s'\n", WiFi.SSID().c_str());
  WiFi.mode(WIFI_STA);
  WiFi.begin(WiFi.SSID().c_str(),WiFi.psk().c_str()); // letzte gespeicherte Zugangsdaten
  int cnt = 0;
  //Wir versuchen eine Anmeldung
  while ((WiFi.status() == WL_DISCONNECTED) && (cnt < 10)){
    delay(500);
    Serial.print(".");
    cnt++;
  }
   wl_status_t status = WiFi.status();
  if(status == WL_CONNECTED) {
    Serial.printf("\nErfolgreich angemeldet an SSID '%s'\n", WiFi.SSID().c_str());
  } else {
    //Wir waren nicht erfolgreich starten daher WPS
    Serial.printf("\nKann keine WiFi Verbindung herstellen. Status ='%d'\n", status); 
    Serial.println("WPS Taste am Router drücken.\n WPS Taste am ESP drücken!");
    if(!startWPS()) {
       delay(300);
       Serial.println("Keine Verbindung über WPS herstellbar");  
    }
  } 
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Attempting MQTT connection...");
    // Create a random client ID

    // Attempt to connect
    if (client.connect(clientID.c_str(),onlineTopic, willQoS, willRetain, willMessage)) {
      Serial.println("connected with Broker");
      client.publish(onlineTopic, isOnlineMessage);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // Wait 5 seconds before retrying
      delay(5000);
    }
  }
}

//Setup Funktion
void setup() {
  Serial.begin(115200); //mit 74880 sind auch die Meldungen beim Start sichtbar
  pinMode(DHTPIN, INPUT); //GPIO04
  pinMode(LDRPIN, INPUT); //GPIO05
  dht.begin();

  setup_wifi();
  client.setServer(mqtt_ip, mqtt_port);
  }


void loop() {
  float dht_humidity = dht.readHumidity();
  float  dht_temperature = dht.readTemperature();
  int brightness = digitalRead(LDRPIN);

  if (!client.connected()) {
    reconnect();
  }
 // client.publish(onlineTopic, isOnlineMessage);
  client.loop();

  //Lichtsensor
  Serial.print("Intensity= ");
  Serial.println(brightness);
  delay(500);

  //DHT22
  if (isnan(dht_humidity) || isnan(dht_temperature)) {
    Serial.println(F("Failed to read from DHT sensor!"));
    }

  //Serial.print("Humidity: ");
 // Serial.println(dht_humidity);
  //Serial.print("Temperature: ");
 // Serial.println(dht_temperature);
  delay(300);
  
  unsigned long now = millis();
  if (now - lastMsg > 60000) {
    lastMsg = now;
    snprintf (msg, MSG_BUFFER_SIZE, "{\"brightness\":\"%d\",\"temperature\":\"%.2f\",\"Humidity\":\"%.2f\" }", brightness, dht_temperature, dht_humidity);
    Serial.print("Publish message: ");
    Serial.println(msg);
    client.publish(topic, msg);
  }

}