import mqttClient as mqttController
import payloadHandler as payloadHandler
import iftttHandler as iftttHandler
import logging
from logging.handlers import TimedRotatingFileHandler

mqtt_broker_ip = "192.168.178.28"
mqtt_broker_port = 1883
mqtt_sub_topic = "/weatherwatch/data/"

"""
create logger
"""
logger = logging.getLogger("weatherWatch")
logger.setLevel(logging.DEBUG)
console_log = logging.StreamHandler()
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
filename = "/var/log/weatherWatch.log"
filelog_handler = TimedRotatingFileHandler(filename, when="midnight", interval=1, backupCount=2)
filelog_handler.setLevel(logging.DEBUG)
filelog_handler.setFormatter(formatter)
#filelog_handler.suffix = "%Y%m%d"


console_log.setFormatter(formatter)
logger.addHandler(filelog_handler)
logger.addHandler(console_log)


def request_to_payloadHandler(sender, request_payload):
    if request_payload is None:
        logger.error("no message received")
    else:
        payload_controller.resolve_payload(ifttt_controller, request_payload)


if __name__ == "__main__":
    print("Weatherwatch controller starting....")
    logger.info("start WeatherwatchController and initalize all components")
    ifttt_controller = iftttHandler.IftttHandler()
    payload_controller = payloadHandler.PayloadHandler()
    subscriber = mqttController.Subscriber(mqtt_broker_ip, mqtt_broker_port, mqtt_sub_topic)
    subscriber.evt_action += request_to_payloadHandler
    subscriber.subscribe()
