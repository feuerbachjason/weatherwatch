import sys
import Adafruit_DHT
import time
import RPi.GPIO as GPIO

sensor_args = { '11': Adafruit_DHT.DHT11,
                '22': Adafruit_DHT.DHT22,
                '2302': Adafruit_DHT.AM2302 }
switcher = { 1: "Dark",
             0: "Bright" }
if len(sys.argv) == 3 and sys.argv[1] in sensor_args:
    sensor = sensor_args[sys.argv[1]]
    pin = sys.argv[2]

    GPIO.setmode(GPIO.BCM)
    GPIO.setup(4, GPIO.IN)

    temperature, humidity = Adafruit_DHT.read_retry(sensor, pin)
    status = GPIO.input(4)

    while True:
        current = GPIO.input(4)
        cTemperature, cHumidity = Adafruit_DHT.read_retry(sensor, pin)
        if(cTemperature != temperature or cHumidity != humidity or current != status):
            sys.stdout.write("\033[F")
            sys.stdout.write("\033[K")
            print('Temp={0:0.1f}*  Humidity={1:0.1f}%  Status={2}'.format(cHumidity, cTemperature, switcher.get(current)))
            temperature = cTemperature
            humidity = cHumidity
            status = current
        time.sleep(0.2)
    
else:
    print('Usage: sudo ./Adafruit_DHT.py [11|22|2302] <GPIO pin number>')
    print('Example: sudo ./Adafruit_DHT.py 2302 4 - Read from an AM2302 connected to GPIO pin #4')
    sys.exit(1)


    
