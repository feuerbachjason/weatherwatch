import paho.mqtt.client as mqtt
import logging

online_topic = "/weatherwatch/online/"


class Event(object):
    """
    Triggers MessageToAction in certwatch if on_message triggered
    """

    def __init__(self):
        self.handlers = []

    def add(self, handler):
        self.handlers.append(handler)
        return self

    def remove(self, handler):
        self.handlers.remove(handler)
        return self

    def fire(self, sender, received_payload):
        for handler in self.handlers:
            handler(sender, received_payload)

    __iadd__ = add
    __isub__ = remove
    __call__ = fire


class Subscriber:

    def __init__(self, mqtt_broker_ip, mqtt_broker_port, mqtt_sub_topic):
        self.logger = logging.getLogger("weatherWatch.mqttClient")
        self.mqtt_broker_ip = mqtt_broker_ip
        self.mqtt_broker_port = mqtt_broker_port
        self.mqtt_sub_topic = mqtt_sub_topic
        self.evt_action = Event()
        #print("Initializes subscriber with mqttID: ", mqtt_broker_ip)
        self.logger.info("Initialize Subscriber on Broker: %s : %s", self.mqtt_broker_ip, self.mqtt_broker_port)

    def subscribe(self):
        def on_connect(client, userdata, flags, rc):
            self.logger.info("Connected Subscriber with result code %s", str(rc))
            client.subscribe(self.mqtt_sub_topic)
            client.subscribe(online_topic)
            self.logger.info("connected to topics: %s and %s", self.mqtt_sub_topic, online_topic)

        def on_message(client, userdata, msg, ):
            received_payload = msg.payload.decode("utf-8")
            if msg.topic == online_topic:
                self.logger.info("Online status has changed: %s", received_payload)
            else:
                self.evt_action(self, received_payload)


        #print("Starting subscriber on topic ", self.mqtt_sub_topic)

        # Initialisiere Subscriber
        client_subscriber = mqtt.Client(client_id="Verwaltungseinheit")
        client_subscriber.on_connect = on_connect
        client_subscriber.on_message = on_message
        client_subscriber.connect(host=self.mqtt_broker_ip, port=self.mqtt_broker_port, keepalive=60)
        client_subscriber.loop_forever()