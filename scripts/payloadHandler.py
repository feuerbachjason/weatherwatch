import payloadToObjectBuilder as payloadToObjectBuilder
from datetime import datetime
import logging


def is_time_to_send_alert():
    now = datetime.now()
    #current_time = now.strftime("%H:%M")
    current_time = now.strftime("%H:%M")
    check_time = ['07:00', '12:00', '16:00', '20:00']
    if current_time in check_time:
        return True
    return False


class PayloadHandler:

    def __init__(self):
        self.logger = logging.getLogger("weatherWatch.payloadHandler")
        #print("Initializes payload handler")
        self.logger.info("initalize payloadHandler")

    def resolve_payload(self, ifttt_controller, request_payload):
        """
            push requestPayload from certwatch to Dispatcher
            """
        #print(request_payload)
        self.logger.info("Weatherdata: %s", request_payload)
        weatherdata_object = payloadToObjectBuilder.PayloadToObjectBuilder(request_payload)
        self.trigger_alert(ifttt_controller, weatherdata_object)
        self.handle_brightness(ifttt_controller, weatherdata_object.brightness)

    def handle_brightness(self, ifttt_controller, brightness):
        """
            handle brightness
            """
        trigger = ""
        if brightness:
            trigger = "lights_on"
            self.logger.info("trigger ifttt-event lights_on")
        elif not brightness:
            trigger = "lights_out"
            self.logger.info("trigger ifttt-event lights_out")
        ifttt_controller.ifttt_trigger_lights(trigger)

    def handle_temperature(self, ifttt_controller, temperature):
        """
            handle temperature
            """

    def trigger_alert(self, ifttt_controller, weatherdata_object):
        trigger = "weather_alert"
        if is_time_to_send_alert():
            self.logger.info("trigger ifttt-event weather_alert")
            ifttt_controller.ifttt_trigger_alert(trigger, weatherdata_object.temperature, weatherdata_object.humidity)
