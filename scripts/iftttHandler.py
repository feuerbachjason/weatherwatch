import requests
import logging

ifttt_logger = logging.getLogger("ifttt_logger")


class IftttHandler:

    def __init__(self):
        self.logger = logging.getLogger("weatherWatch.iftttHandler")
        #print("Initializing iftttController to handle requests...")
        self.logger.info("Initializes iftttController")

    def ifttt_trigger_lights(self, trigger):
        url_to_light = "https://maker.ifttt.com/trigger/{0}/with/key/" \
                "dWb2bJe4jTzTSoI6YUluRCR9Rjd3nnyu-Jk59YSbRBR".format(trigger)
        #print("Sending request to IFTTT")
        #self.logger.info("Sending request to IFTTT")
        response = requests.post(url_to_light)
        if response.status_code == 200:
            self.logger.info("Sending request to IFTTT successfully")
        else:
            self.logger.info("Failed to send request. Responsecode: %s", response.status_code)


    def ifttt_trigger_alert(self, trigger, temperature, humidity):
        url_to_alert = "https://maker.ifttt.com/trigger/{0}/with/key/" \
                "dWb2bJe4jTzTSoI6YUluRCR9Rjd3nnyu-Jk59YSbRBR" \
                "?value1={1}&value2={2}".format(trigger, temperature, humidity)
        self.logger.info("trigger alert")
        requests.post(url_to_alert)
