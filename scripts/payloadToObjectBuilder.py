from distutils.util import strtobool
import json
import logging

class PayloadToObjectBuilder:
	"""
	Generates weatherobject from jsonPayload
	"""
	def __init__(self, request_payload):
		self.logger = logging.getLogger("weatherWatch.PayloadToObjectBuilder")
		try:
			request_payload_as_json = json.loads(request_payload)
			self.brightness = strtobool(request_payload_as_json.get("brightness"))
			self.temperature = float(request_payload_as_json.get("temperature"))
			self.humidity = float(request_payload_as_json.get("humidity"))
			self.logger.info("Mapped Payload to JSON-Object successfully")
		except ValueError:
			error_message = "Failed to convert payload into json-document"
			print(error_message)
			self.logger.error(error_message)


	def __lt__(self, other):
		return self.brightness < other.brightness
