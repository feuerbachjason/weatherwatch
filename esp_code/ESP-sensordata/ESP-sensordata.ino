/*
Author: Jason Feuerbach, Jan Knierim, Sebastian Wieczorek
Pushing sensordata to mqtt
*/
#include "ESP8266WiFi.h"
#include <PubSubClient.h>
#include "DHT.h"


//Pin Ausgänge
#define DHTPIN 14 //GPIO14 (PIN D5)
#define LDRPIN 5  //GPIO5 (PIN D1)
#define DHTTYPE DHT22
#define MSG_BUFFER_SIZE  (80)

//Konstanten und Variablen
String clientID = "ESP_Wetterstation";
const char* mqtt_ip = "raspberrypi.fritz.box";
const int mqtt_port = 1883;
const char* topic = "/weatherwatch/data/";
unsigned long lastMsg = 0;
char msg[MSG_BUFFER_SIZE];
const char* onlineTopic = "/weatherwatch/online/";
const char* willMessage = "{\"status\": \"0\"}";
const char* isOnlineMessage = "{\"status\": \"1\"}";
byte willQoS = 0;
bool willRetain = true;
bool wifiConnect;
//Creates objects
WiFiClient espClient;
PubSubClient client(espClient);
DHT dht(DHTPIN, DHTTYPE);

//Starts WPS Konfiguration
bool startWPS() {
  Serial.println("WPS Konfiguration gestartet");
  bool wpsSuccess = WiFi.beginWPSConfig();
  if(wpsSuccess) {
      // Muss nicht immer erfolgreich sein! Nach einem Timeout ist die SSID leer
      String newSSID = WiFi.SSID();
      if(newSSID.length() > 0) {
        // Nur erfolgreich, wenn eine SSID gefunden wurde
        Serial.printf("WPS abgeschlossen. Erfolgreich angemeldet an SSID '%s'\n", newSSID.c_str());
      } else {
        wpsSuccess = false;
      }
  }
  return wpsSuccess; 
}

void setup_wifi(){
  Serial.printf("\nVersuche Verbindung mit gespeicherter SSID '%s'\n", WiFi.SSID().c_str());
  WiFi.mode(WIFI_STA);
  WiFi.begin(WiFi.SSID().c_str(),WiFi.psk().c_str()); // letzte gespeicherte Zugangsdaten
  int cnt = 0;
  //Anmeldungsversuch
  while ((WiFi.status() == WL_DISCONNECTED) && (cnt < 10)){
    delay(500);
    Serial.print(".");
    cnt++;
  }
   wl_status_t status = WiFi.status();
  if(status == WL_CONNECTED) {
    Serial.printf("Erfolgreich angemeldet an SSID '%s'\n", WiFi.SSID().c_str());
    wifiConnect = true;
  } else {
    //Wir waren nicht erfolgreich starten daher WPS
    Serial.printf("Kann keine WiFi Verbindung herstellen. Status ='%d'\n", status); 
    Serial.println("WPS Taste am Router drücken.\n WPS Taste am ESP drücken!");
    if(!startWPS()) {
       delay(300);
       Serial.println("Keine Verbindung per WPS möglich");  
    }
  } 
}
void reconnect() {
  // Loop until we're reconnected
  while (!client.connected()) {
    Serial.print("Versuche MQTT Verbindung aufzubauen...");

    // Attempt to connect
    if (client.connect(clientID.c_str(),onlineTopic, willQoS, willRetain, willMessage)) {
      Serial.println("connected with Broker");
      client.publish(onlineTopic, isOnlineMessage);
    } else {
      Serial.print("failed, rc=");
      Serial.print(client.state());
      Serial.println(" try again in 5 seconds");
      // warte 5 Sekunden vor einem erneuten Versuch
      delay(5000);
    }
  }
}

//Setup Funktion
void setup() {
  Serial.begin(74880); //mit 74880 sind auch die Meldungen beim Start sichtbar. 115200 nicht
  pinMode(DHTPIN, INPUT); //GPIO02
  pinMode(LDRPIN, INPUT); //GPIO14
  pinMode(BUILTIN_LED, OUTPUT);     //  BUILTIN_LED pin als Ausgang definieren
  dht.begin();
  setup_wifi();
  client.setServer(mqtt_ip, mqtt_port);
  }


void loop() {
  float dht_humidity = dht.readHumidity();
  float dht_temperature = dht.readTemperature();
  int brightness = digitalRead(LDRPIN);
  
  if (client.connected() && wifiConnect) {
    digitalWrite(LED_BUILTIN, LOW);
  }
  else {
    digitalWrite(LED_BUILTIN, HIGH);
    reconnect();
  }
  client.loop();
  unsigned long now = millis();
  if (now - lastMsg > 60000) {
    lastMsg = now;
    if (isnan(dht_humidity) || isnan(dht_temperature)) {
      Serial.println(F("Failed to read from DHT sensor!"));
      }
    snprintf (msg, MSG_BUFFER_SIZE, 
              "{\"brightness\":\"%d\",\"temperature\":\"%.2f\",\"humidity\":\"%.2f\" }",
              brightness, dht_temperature, dht_humidity);
    Serial.print("Publishing message: ");
    Serial.println(msg);
    client.publish(topic, msg);
    }
 }
