FROM python:3.8.5-slim
EXPOSE 1883

CMD ["/bin/bash"]
ADD scripts/ /etc/weatherwatch/
WORKDIR /etc/weatherwatch/
RUN apt-get update && \
    apt-get install -y mosquitto && \
    pip3 install paho-mqtt requests

CMD ["python3", "./weatherwatchController.py"]